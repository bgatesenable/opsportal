<?php

	/**
		* @page Index
		*
		* @author Billy Gates <bgates@enabledev.co.uk>
		*
		* @brief Handle website requests and serve the correct page
		*
		* This page is designed to handle the requests by locating and serving the correct pages based on URL parameters. 
		* This relies heavily on the `.htaccess` being configured correctly.
	*/

	define('_enableFramework', 1);

	session_start();

	require_once '/var/www/opsConfig/opsConfig.php';

	if (isset($_SESSION['last_action'])) {
		$secondsInactive = time() - $_SESSION['last_action'];
		if($secondsInactive >= (60 * 30)){
        	session_unset();
        	session_destroy();
			$_SESSION = array();
		}
	}

	$_SESSION['last_action'] = time();

	include 'resources/header.php';

	if (isset($_GET['view'])) {
		if (file_exists('views/' . $_GET['view'] . '.php')) {
			include 'views/' . $_GET['view'] . '.php';
		} else {
			header('Location: /notFound/404');
		}
	} else {
		header('Location: /default');
	}

	include 'resources/footer.php';
?>